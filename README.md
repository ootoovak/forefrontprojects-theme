# Forefront Projects Theme
Forefront Projects Theme is a [Gutenberg](https://github.com/Keats/gutenberg) theme used for the [Forefront Projects](https://forefrontprojects.com) website.

## Contents

- [Installation](#installation)
- [Options](#options)

## Installation
First download this theme to your `themes` directory:

```bash
$ cd themes
$ git clone https://gitlab.com/ootoovak/forefrontprojects-theme
```
and then enable it in your `config.toml`:

```toml
theme = "forefrontprojects-theme"
```

## Options